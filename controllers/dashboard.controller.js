const Dashboard = require("../models/dashboard.model.js")

exports.getData = (req, res) => {
    Dashboard.getData((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving data."
            });
        else res.send(data);
    });
}