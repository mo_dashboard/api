const User = require("../models/user.model.js");

exports.createUser = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    const user = new User({
        email: req.body.email,
        full_name: req.body.full_name,
        role_id: req.body.role_id,
        mobile_number: req.body.mobile_number
    });

    User.createUser(user, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Activity."
            });
        else res.send(data);
    });
}

exports.getAllUser = (req, res) => {
    User.getAllUser((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving activities."
            });
        else res.send(data);
    });
}

exports.getByEmail = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    User.getByEmail(req.body.email, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: 'Activity not found'
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Activity"
                });
            }
        } else res.send(data);
    });
}

exports.updateUser = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    const user = new User({
        // email: req.body.email,
        full_name: req.body.full_name,
        role_id: req.body.role_id,
        mobile_number: req.body.mobile_number
    });

    User.updateUser(req.body.email, new User(req.body),  (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: 'User with email ' + req.body.email + ' not found'
                });
            } else {
                res.status(500).send({
                    message: "Error updating User with email " + req.body.email
                });
            }
        } else res.send(data);
    });
}

exports.deleteUserByEmail = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    User.deleteUserByEmail(req.body.email, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Not found User with email " + req.body.email
                });
            } else {
                res.status(500).send({
                    message: "Could not delete User with email " + req.body.email
                });
            }
        } else res.send({ message: `User was deleted successfully!` });
    });
}

exports.getStatYear = (req, res) => {
    if(!req.params) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    User.getStatYear(req.params.yearIn, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Not found User with email " + req.params.yearIn
                });
            } else {
                res.status(500).send({
                    message: "Could not delete User with email " + req.params.yearIn
                });
            }
        } else res.send(data);
    })
}
