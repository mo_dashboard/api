const Activity = require("../models/activity.model.js");

exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    const activity = new Activity({
        report_time: req.body.report_time,
        working_unit: req.body.working_unit,
        project_id: req.body.project_id,
        activity: req.body.activity,
        request_source: req.body.request_source,
        Activity_pic_email: req.body.customer_pic_email,
        internal_pic_email: req.body.internal_pic_email,
        internal_reviewer_email: req.body.internal_reviewer_email,
        action_time: req.body.action_time,
        need_approval: req.body.need_approval
        // approval_status: req.body.approval_status
    });

    Activity.create(req.body.report_time,
        req.body.working_unit,
        req.body.project_id,
        req.body.activity,
        req.body.request_source,
        req.body.customer_pic_email,
        req.body.internal_pic_email,
        req.body.internal_reviewer_email,
        req.body.action_time,
        req.body.need_approval,
        // req.body.approval_status, 
        (err, data) => {
            if (err)
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while creating the Activity."
                });
            else res.send(data);
        });

};

exports.getAll = (req, res) => {
    Activity.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving activities."
            });
        else res.send(data);
    });
}

exports.getById = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Activity.getById(req.body.activity_id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: 'Activity not found'
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Activity"
                });
            }
        } else res.send(data);
    });
}

exports.getByIntPic = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Activity.getByIntPic(req.body.internal_pic_email, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: 'Activity not found.'
                });
                // console.log(req.body)
            } else {
                res.status(500).send({
                    message: "Error retrieving Activity."
                });
            }
        } else res.send(data);
    });
}

exports.getByIntReviewer = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Activity.getByIntReviewer(req.body.internal_reviewer_email, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: 'Not found Activity with internal Reviewer ' + req.body.internal_reviewer_email
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Activity with internal Reviewer " + req.body.internal_reviewer_email
                });
            }
        } else res.send(data);
    });
}

exports.getByProject = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Activity.getByProject(req.body.project_id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: 'Not found Activity with internal Reviewer ' + req.body.project_id
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Activity with internal Reviewer " + req.body.project_id
                });
            }
        } else res.send(data);
    });
}

exports.needAproval = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    Activity.needAproval(req.body.internal_reviewer_email, req.body.approval_status, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: 'Not found Activity.'
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Activity"
                });
            }
        } else res.send(data);
    })
}

exports.approve = (req, res) => {
    // Validate Request
    if (!req.params) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Activity.approve(req.params.acitvivtyId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Activity with id ${req.params.acitvivtyId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error approving Activity with id " + req.params.acitvivtyId
                });
            }
        } else res.send(data);
    });
}

exports.reject = (req, res) => {
    // Validate Request
    if (!req.params) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Activity.reject(req.params.acitvivtyId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Activity with id ${req.params.acitvivtyId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error rejecting Activity with id " + req.params.acitvivtyId
                });
            }
        } else res.send(data);
    });
}

exports.getStat = (req, res) => {
    Activity.getStat((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving activities."
            });
        else res.send(data);
    });
}

exports.getStatYear = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Activity.getStatYear(req.params.yearIn, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Activity with id ${req.params.yearIn}.`
                });
            } else {
                res.status(500).send({
                    message: "Error rejecting Activity with id " + req.params.yearIn
                });
            }
        } else res.send(data);
    });
}

exports.getStatIntPICAll = (req, res) => {

    Activity.getStatIntPICAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving activities."
            });
        else res.send(data);
    });
}

exports.getStatIntPIC = (req, res) => {
    // Validate Request
    if (!req.params) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Activity.getStatIntPIC(req.params.picEmail, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Activity with id ${req.params.picEmail}.`
                });
            } else {
                res.status(500).send({
                    message: "Error rejecting Activity with id " + req.params.picEmail
                });
            }
        } else res.send(data);
    });
}
