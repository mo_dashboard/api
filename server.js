const express = require("express");
const app = express();
const cors= require("cors");

// enable all cors
app.use(cors());

// parse requests of content-type: application/json
app.use(express.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// simple route
app.get("/api", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

require("./routes/user.routes.js")(app);
require("./routes/activity.routes.js")(app);
require("./routes/dashboard.routes.js")(app);
// set port, listen for requests
app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});