module.exports = app => {
    const users = require("../controllers/user.controller.js");

    app.post("/api/user/ins", users.createUser);

    app.get("/api/user/getAll", users.getAllUser);

    app.post("/api/user/getByEmail", users.getByEmail);

    app.post("/api/user/update", users.updateUser);

    app.post("/api/user/delete", users.deleteUserByEmail);

    app.get("/api/user/statistic/getStatYear/:yearIn", users.getStatYear);
}