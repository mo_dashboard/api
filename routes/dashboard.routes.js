module.exports = app => {

    const dashboard = require("../controllers/dashboard.controller.js");

    app.get("/api/dashboard/getData", dashboard.getData);
}