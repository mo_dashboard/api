module.exports = app => {
    
    const activities = require("../controllers/activity.controller.js");

    app.post("/api/activity/ins", activities.create);

    app.get("/api/activity/getAll", activities.getAll);

    app.post("/api/activity/getById", activities.getById);

    app.post("/api/activity/getByIntPic", activities.getByIntPic);

    app.post("/api/activity/getByRev", activities.getByIntReviewer);

    app.post("/api/activity/getByProject", activities.getByProject);

    app.post("/api/activity/approval", activities.needAproval);

    app.post("/api/activity/approval/approve/:acitvivtyId", activities.approve);

    app.post("/api/activity/approval/reject/:acitvivtyId", activities.reject);

    app.get("/api/activity/statistic/getStat", activities.getStat);

    app.get("/api/activity/statistic/getStatYear/:yearIn", activities.getStatYear);

    app.get("/api/activity/statistic/bypic", activities.getStatIntPICAll);

    app.get("/api/activity/statistic/bypic/:picEmail", activities.getStatIntPIC);
}