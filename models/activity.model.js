const { reject } = require("async");
const sql = require("./db.js");
// const async = require('async')

// constructor
const Activity = function (activity) {
    this.activity_id = activity.activity_id;
    this.report_time = activity.report_time;
    this.working_unit = activity.working_unit;
    this.project_id = activity.project_id;
    this.activity = activity.activity;
    this.request_source = activity.request_source;
    this.customer_pic_email = activity.customer_pic_email;
    this.internal_pic_email = activity.internal_pic_email;
    this.internal_reviewer_email = activity.internal_reviewer_email;
    this.action_time = activity.action_time;
    this.need_approval = activity.need_approval;
    this.approval_status = activity.approval_status;
    this.approval_time = activity.approval_time;
    this.project_name = activity.project_name;
}

Activity.create = (
    reportTime,
    workingUnit,
    projectId,
    activityDesc,
    reportSource,
    custEmail,
    intPICEmail,
    intReviewerEmail,
    actionTime,
    needAproval,
    // approvalStatus,
    result) => {
    if (needAproval === 0) {
        sql.query("INSERT INTO mo_daily_activity(approval_status ,report_time, working_unit, project_id, activity, request_source, customer_pic_email, internal_pic_email, internal_reviewer_email, action_time, need_approval) VALUES ('-',?,?,?,?,?,?,?,?,?,?) ",
            [
                reportTime,
                workingUnit,
                projectId,
                activityDesc,
                reportSource,
                custEmail,
                intPICEmail,
                intReviewerEmail,
                actionTime,
                needAproval,
                // approvalStatus,
            ], (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                console.log("created activity: ", { id: res.insertId });
                result(null,
                    {
                        success: true,
                        message: "Acitvity Created",
                        id: res.insertId,
                    });
            });
    } else if (needAproval === 1) {
        sql.query("INSERT INTO mo_daily_activity(approval_status ,report_time, working_unit, project_id, activity, request_source, customer_pic_email, internal_pic_email, internal_reviewer_email, action_time, need_approval) VALUES ('waiting_approval',?,?,?,?,?,?,?,?,?,?) ",
            [
                reportTime,
                workingUnit,
                projectId,
                activityDesc,
                reportSource,
                custEmail,
                intPICEmail,
                intReviewerEmail,
                actionTime,
                needAproval,
                // approvalStatus,
            ], (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }
                console.log("created activity: ", { id: res.insertId });
                result(null,
                    {
                        success: true,
                        message: "Acitvity Created",
                        id: res.insertId,
                    });
            });
    }
}

Activity.getAll = result => {
    sql.query("SELECT mo_daily_activity.activity_id, mo_daily_activity.report_time, mo_daily_activity.working_unit, mo_project.project_name,mo_daily_activity.activity, mo_daily_activity.request_source, mo_daily_activity.customer_pic_email, mo_daily_activity.internal_pic_email, mo_daily_activity.internal_reviewer_email, mo_daily_activity.action_time, mo_daily_activity.need_approval, mo_daily_activity.approval_status, mo_daily_activity.approval_time FROM mo_daily_activity JOIN mo_project on mo_project.project_id = mo_daily_activity.project_id ORDER BY mo_daily_activity.report_time DESC", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log("acivities: ", res);
        result(null, res);
    });
}

Activity.getById = (getID, result) => {
    sql.query("SELECT mo_daily_activity.activity_id, mo_daily_activity.report_time, mo_daily_activity.working_unit, mo_project.project_name,mo_daily_activity.activity, mo_daily_activity.request_source, mo_daily_activity.customer_pic_email, mo_daily_activity.internal_pic_email, mo_daily_activity.internal_reviewer_email, mo_daily_activity.action_time, mo_daily_activity.need_approval, mo_daily_activity.approval_status, mo_daily_activity.approval_time FROM mo_daily_activity JOIN mo_project on mo_project.project_id = mo_daily_activity.project_id WHERE mo_daily_activity.activity_id = ?", [getID], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found activity : ", res);
            result(null, res);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
}

Activity.getByIntPic = (intPicEmail, result) => {
    // let query = sql.query("SELECT * FROM mo_daily_activity WHERE internal_pic_email = ?", [intPicEmail]);
    sql.query("SELECT mo_daily_activity.activity_id, mo_daily_activity.report_time, mo_daily_activity.working_unit, mo_project.project_name,mo_daily_activity.activity, mo_daily_activity.request_source, mo_daily_activity.customer_pic_email, mo_daily_activity.internal_pic_email, mo_daily_activity.internal_reviewer_email, mo_daily_activity.action_time, mo_daily_activity.need_approval, mo_daily_activity.approval_status, mo_daily_activity.approval_time FROM mo_daily_activity JOIN mo_project on mo_project.project_id = mo_daily_activity.project_id WHERE mo_daily_activity.internal_pic_email = ? ORDER BY mo_daily_activity.report_time DESC", [intPicEmail], (err, res) => {
        // console.log(query)
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found activity : ", res);
            result(null, res);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
}

Activity.getByIntReviewer = (reviewerEmail, result) => {
    sql.query("SELECT mo_daily_activity.activity_id, mo_daily_activity.report_time, mo_daily_activity.working_unit, mo_project.project_name,mo_daily_activity.activity, mo_daily_activity.request_source, mo_daily_activity.customer_pic_email, mo_daily_activity.internal_pic_email, mo_daily_activity.internal_reviewer_email, mo_daily_activity.action_time, mo_daily_activity.need_approval, mo_daily_activity.approval_status, mo_daily_activity.approval_time FROM mo_daily_activity JOIN mo_project on mo_project.project_id = mo_daily_activity.project_id mo_daily_activity.internal_reviewer_email = ? ORDER BY mo_daily_activity.report_time DESC", [reviewerEmail], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found activity : ", res);
            result(null, res);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
}

Activity.getByProject = (projectID, result) => {
    sql.query("SELECT mo_daily_activity.activity_id, mo_daily_activity.report_time, mo_daily_activity.working_unit, mo_project.project_name,mo_daily_activity.activity, mo_daily_activity.request_source, mo_daily_activity.customer_pic_email, mo_daily_activity.internal_pic_email, mo_daily_activity.internal_reviewer_email, mo_daily_activity.action_time, mo_daily_activity.need_approval, mo_daily_activity.approval_status, mo_daily_activity.approval_time FROM mo_daily_activity JOIN mo_project on mo_project.project_id = mo_daily_activity.project_id WHERE mo_daily_activity.project_id = ?", [projectID], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found by project activity : ", res);
            result(null, res);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
}

Activity.needAproval = (reviewerEmail, approvalStatus, result) => {
    sql.query(
        "SELECT mo_daily_activity.activity_id, mo_daily_activity.report_time, mo_daily_activity.working_unit, mo_project.project_name,mo_daily_activity.activity, mo_daily_activity.request_source, mo_daily_activity.customer_pic_email, mo_daily_activity.internal_pic_email, mo_daily_activity.internal_reviewer_email, mo_daily_activity.action_time, mo_daily_activity.need_approval, mo_daily_activity.approval_status, mo_daily_activity.approval_time FROM mo_daily_activity JOIN mo_project on mo_project.project_id = mo_daily_activity.project_id WHERE mo_daily_activity.internal_reviewer_email = ? AND mo_daily_activity.need_approval = 1 AND mo_daily_activity.approval_status = ? ORDER BY mo_daily_activity.report_time DESC",
        [reviewerEmail, approvalStatus], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            if (res.length) {
                console.log("found activity : ", res);
                result(null, res);
                return;
            }

            // not found Customer with the id
            result({ kind: "not_found" }, null);
        })
}

Activity.approve = (acitvivtyId, result) => {
    sql.query(
        "SELECT approval_status FROM mo_daily_activity WHERE activity_id = ?",
        [acitvivtyId], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }
            // console.log(res[0].approval_status);
            if (res[0].approval_status === "waiting_approval") {
                sql.query(
                    "UPDATE mo_daily_activity SET approval_status = 'approved', approval_time = now() where activity_id = ?",
                    [acitvivtyId], (err, res) => {
                        if (err) {
                            console.log("error: ", err);
                            result(null, err);
                            return;
                        }

                        if (res.affectedRows == 0) {
                            // not found Customer with the id
                            result({ kind: "not_found" }, null);
                            return;
                        }

                        console.log("Approve activity: ",
                            {
                                success: true,
                                message: "Approve success"
                            });
                        result(null,
                            {
                                success: true,
                                message: "Approve success"
                            });
                    });
            } else {
                console.log("Approve activity: ",
                    {
                        success: false,
                        message: "Invalid Activity Approval Status"
                    });
                result(null,
                    {
                        success: false,
                        message: "Invalid Activity Approval Status"
                    });
            }
        });

}

Activity.reject = (acitvivtyId, result) => {
    sql.query(
        "SELECT approval_status FROM mo_daily_activity WHERE activity_id = ?",
        [acitvivtyId], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res[0].approval_status === "waiting_approval") {
                sql.query(
                    "UPDATE mo_daily_activity SET approval_status = 'rejected', approval_time = now() where activity_id = ?",
                    [acitvivtyId], (err, res) => {
                        if (err) {
                            console.log("error: ", err);
                            result(null, err);
                            return;
                        }

                        if (res.affectedRows == 0) {
                            // not found Customer with the id
                            result({ kind: "not_found" }, null);
                            return;
                        }

                        console.log("reject activity: ",
                            {
                                success: true,
                                message: "Reject success"
                            });
                        result(null,
                            {
                                success: true,
                                message: "Reject success"
                            });
                    });
            } else {
                console.log("Approve activity: ",
                    {
                        success: false,
                        message: "Invalid Activity Approval Status"
                    });
                result(null,
                    {
                        success: false,
                        message: "Invalid Activity Approval Status"
                    });
            }
        });
}

Activity.getStat = result => {
    sql.query("SELECT COUNT(mo_daily_activity.activity_id) AS total_activity, MONTHNAME(mo_daily_activity.report_time) AS month, YEAR(mo_daily_activity.report_time) AS year, mo_project.project_name FROM mo_daily_activity JOIN mo_project ON mo_daily_activity.project_id = mo_project. project_id GROUP BY YEAR(report_time), MONTHNAME(report_time), mo_project.project_name",
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            // console.log("acivities stat: ", res);
            result(null, res);
        });
}

Activity.getStatYear = (yearIn, result) => {
    var statRes = []
    var monthlyData = []
    var activityCount = new Promise((resolve, reject) => {
        sql.query("SELECT project_id, project_name FROM mo_project",  (err, res) => {

            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }
            // console.log("res: ", res.length);  
            // projects = res;
            // result(null, res);
            if ((err) => {
                reject(err)
            })
            resolve(res)
        });
    })

    activityCount.then((res) => {
        console.log('##### projects: ' + JSON.stringify(res))
        // result(null, res)
        var data = new Promise((resolve, reject) => {
            var projectStat = []
            for (let i in res) {                
                // projectStat.push({
                //     project_name: res[i].project_name,
                //     year: yearIn,
                // })

                // monthlyData.push(projects[i].project_name) 
                // console.log('##### projects: ' + projects[i].project_id)
                let getStatQuery = 'select coalesce(sum(mo_daily_activity.project_id =  "' + res[i].project_id + '"),0) activity_count,y,m ' +
                    'from  ' +
                    '(select 1 as m,' + yearIn + ' as y ' +
                    'union select 2 as m,' + yearIn + ' as y ' +
                    'union select 3 as m,' + yearIn + ' as y ' +
                    'union select 4 as m,' + yearIn + ' as y ' +
                    'union select 5 as m,' + yearIn + ' as y ' +
                    'union select 6 as m,' + yearIn + ' as y ' +
                    'union select 7 as m,' + yearIn + ' as y ' +
                    'union select 8 as m,' + yearIn + ' as y ' +
                    'union select 9 as m,' + yearIn + ' as y ' +
                    'union select 10 as m,' + yearIn + ' as y ' +
                    'union select 11 as m,' + yearIn + ' as y ' +
                    'union select 12 as m ,' + yearIn + ' as y) months ' +
                    'left join mo_daily_activity on(months.m = month(mo_daily_activity.report_time) and months.y = year(mo_daily_activity.report_time)) ' +
                    'left JOIN mo_project on mo_daily_activity.project_id = mo_project.project_id ' +
                    // 'WHERE YEAR(mo_daily_activity.report_time) = ? ' +
                    'group by months.m, months.y, year(mo_daily_activity.report_time);'
                // console.log("query result: " + getStatQuery)
                sql.query(getStatQuery, async (err, res2) => {
                    if (err) {
                        // console.log("error: ", err);
                        result(null, err);
                        return;
                    }
                    statRes = await res2

                    monthlyData = await statRes.map(function (el) {
                        return el.activity_count;
                    });
                    projectStat.push({
                        project_name: res[i].project_name,
                        year: yearIn,
                    })
                    projectStat[i]['monthlyData'] = await monthlyData
                    // console.log('##### monthlyDataInLoop: ' + monthlyData)
                    // console.log('##### project stat2: ' + JSON.stringify(projectStat))
                    // activityCount = projectStat
                    // console.log('##### activityCount1: ' + JSON.stringify(activityCount))
                    if (i==res.length -1){
                        if ((err) => {
                            reject(err)
                        })
                        resolve(projectStat)
                    }
                });
                // console.log('##### project stat3: ' + JSON.stringify(projectStat))
                // await console.log('##### statRes: ' + JSON.stringify(statRes))
            }

            // resolve(Promise.all(projects))
        })

        data.then((res) => {
            console.log('##### final: ' + JSON.stringify(res))
            result(null, res)
        })
    })
}

Activity.getStatIntPICAll = (result) => {
    sql.query("SELECT COUNT(mo_daily_activity.activity_id) AS total_activity, MONTHNAME(mo_daily_activity.report_time) AS month, YEAR(mo_daily_activity.report_time) AS year, mo_project.project_name, users.full_name FROM mo_daily_activity JOIN mo_project ON mo_daily_activity.project_id = mo_project. project_id JOIN users ON mo_daily_activity.internal_pic_email = users.email GROUP BY YEAR(mo_daily_activity.report_time), MONTHNAME(mo_daily_activity.report_time), mo_project.project_name, users.full_name",
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            console.log("acivities stat: ", res);
            result(null, res);
        });
}

Activity.getStatIntPIC = (picEmail, result) => {
    sql.query("SELECT COUNT(mo_daily_activity.activity_id) AS total_activity, MONTHNAME(mo_daily_activity.report_time) AS month, YEAR(mo_daily_activity.report_time) AS year, mo_project.project_name, users.full_name FROM mo_daily_activity JOIN mo_project ON mo_daily_activity.project_id = mo_project. project_id JOIN users ON mo_daily_activity.internal_pic_email = users.email WHERE mo_daily_activity.internal_pic_email = ? GROUP BY YEAR(mo_daily_activity.report_time), MONTHNAME(mo_daily_activity.report_time), mo_project.project_name, users.full_name",
        [picEmail], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            console.log("acivities stat: ", res);
            result(null, res);
        });
}

module.exports = Activity;