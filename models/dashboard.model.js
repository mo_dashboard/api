const sql = require("./db.js");

const Dashboard = function (dashboard) {
    this.total_activity = dashboard.total_activity;
    this.total_activity_need_approval = dashboard.total_activity_need_approval;
    this.total_activity_approved = dashboard.total_activity_approved;
    this.total_activity_rejected = dashboard.total_activity_rejected;
    this.total_user = dashboard.total_user;
    this.total_project = dashboard.total_project;
}


Dashboard.getData = result => {
    // var queries = ["SELECT COUNT(*) as total_activity FROM mo_daily_activity",
    //     "SELECT COUNT(*) as total_activity_need_approval FROM mo_daily_activity WHERE need_approval = 1 and approval_status = 'waiting_approval'",
    //     "SELECT COUNT(*) as total_activity_approved FROM mo_daily_activity WHERE need_approval = 1 and approval_status = 'approved'",
    //     "SELECT COUNT(*) as total_activity_rejected FROM mo_daily_activity WHERE need_approval = 1 and approval_status = 'rejected'",
    //     "SELECT COUNT(*) as total_user FROM users",
    //     "SELECT COUNT(*) as total_project FROM mo_project"
    // ]

    sql.query("SELECT COUNT(*) as total_activity FROM mo_daily_activity; SELECT COUNT(*) as total_activity_need_approval FROM mo_daily_activity WHERE need_approval = 1 and approval_status = 'waiting_approval'; SELECT COUNT(*) as total_activity_approved FROM mo_daily_activity WHERE need_approval = 1 and approval_status = 'approved'; SELECT COUNT(*) as total_activity_rejected FROM mo_daily_activity WHERE need_approval = 1 and approval_status = 'rejected'; SELECT COUNT(*) as total_user FROM users; SELECT COUNT(*) as total_project FROM mo_project",
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            console.log("acivities: ", res);
            result(null, {
                total_activity: res[0][0].total_activity,
                total_activity_need_approval: res[1][0].total_activity_need_approval,
                total_activity_approved: res[2][0].total_activity_approved,
                total_activity_rejected: res[3][0].total_activity_rejected,
                total_user: res[4][0].total_user,
                total_project: res[5][0].total_project
            });
            return;
        });
}


module.exports = Dashboard;

