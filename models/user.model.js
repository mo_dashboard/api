const sql = require("./db.js");

// constructor
const User = function (user) {
    this.email = user.email;
    this.full_name = user.full_name;
    this.role_id = user.role_id;
    this.mobile_number = user.mobile_number;
}

let memberStat = []

User.createUser = (newUser, result) => {
    sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        console.log("created user: ", { insertedRow: res.affectedRows, ...newUser });
        result(null,
            {
                success: true,
                message: "User Created",
                ...newUser
            });
    });
}

User.getAllUser = result => {
    sql.query(
        "SELECT users.email, users.full_name, users.mobile_number, users.role_id, role.role_name FROM users, role WHERE users.role_id = role.role_id",
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            console.log("Users: ", res);
            result(null, res);
        });
}

User.getByEmail = (uEmail, result) => {
    sql.query(
        "SELECT users.email, users.full_name, users.mobile_number, users.role_id, role.role_name FROM users, role WHERE users.role_id = role.role_id AND users.email = ?",
        [uEmail], (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            if (res.length) {
                console.log("found User : ", res);
                result(null, res);
                return;
            }

            // not found Customer with the id
            result({ kind: "not_found" }, null);
        });
}

User.updateUser = (uEmail, user, result) => {
    sql.query("UPDATE users SET full_name = ?, role_id = ?, mobile_number = ? WHERE email = ?", 
    [user.full_name, user.role_id, user.mobile_number, uEmail], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Customer with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("User Update: ",
            {
                success: true,
                message: "User success updated"
            });
        result(null,
            {
                success: true,
                message: "User success updated"
            });
    });
}

User.deleteUserByEmail = (uEmail, result) => {
    sql.query("DELETE FROM users WHERE email = ?", [uEmail], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Customer with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted user with email: ", uEmail);
        result(null, 
            {
                success: true,
                message: "User with email " + uEmail + " deleted."
            });

    });
}

User.getStatYear = (yearIn, result) => {
    var statRes = []
    var monthlyData = []
    var activityCount = new Promise((resolve, reject) => {
        sql.query("SELECT email, full_name FROM users JOIN role ON users.role_id = role.role_id WHERE role.role_id = 102",  (err, res) => {

            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }
            if ((err) => {
                reject(err)
            })
            resolve(res)
        });
    })

    activityCount.then((res) => {
        console.log('##### Team Members: ' + JSON.stringify(res))
        // result(null, res)
        var data = new Promise((resolve, reject) => {
            var projectStat = []
            for (let i in res) { 
                let getStatQuery = 'select coalesce(sum(mo_daily_activity.internal_pic_email =  "' + res[i].email + '"),0) activity_count,y,m ' +
                    'from  ' +
                    '(select 1 as m,' + yearIn + ' as y ' +
                    'union select 2 as m,' + yearIn + ' as y ' +
                    'union select 3 as m,' + yearIn + ' as y ' +
                    'union select 4 as m,' + yearIn + ' as y ' +
                    'union select 5 as m,' + yearIn + ' as y ' +
                    'union select 6 as m,' + yearIn + ' as y ' +
                    'union select 7 as m,' + yearIn + ' as y ' +
                    'union select 8 as m,' + yearIn + ' as y ' +
                    'union select 9 as m,' + yearIn + ' as y ' +
                    'union select 10 as m,' + yearIn + ' as y ' +
                    'union select 11 as m,' + yearIn + ' as y ' +
                    'union select 12 as m ,' + yearIn + ' as y) months ' +
                    'left join mo_daily_activity on(months.m = month(mo_daily_activity.report_time) and months.y = year(mo_daily_activity.report_time)) ' +
                    'left JOIN users on mo_daily_activity.internal_pic_email = users.email ' +
                    'group by months.m, months.y, year(mo_daily_activity.report_time);'
                sql.query(getStatQuery, async (err, res2) => {
                    if (err) {
                        result(null, err);
                        return;
                    }
                    statRes = await res2

                    monthlyData = await statRes.map(function (el) {
                        return el.activity_count;
                    });
                    projectStat.push({
                        member_name: res[i].full_name,
                        year: yearIn,
                    })
                    projectStat[i]['monthlyData'] = await monthlyData
                    if (i==res.length -1){
                        if ((err) => {
                            reject(err)
                        })
                        resolve(projectStat)
                    }
                });
            }
        })

        data.then((res) => {
            console.log('##### final: ' + JSON.stringify(res))
            result(null, res)
        })
    })
}

module.exports = User;